package com.example.ex7_asynctask;

import android.os.AsyncTask;
import android.widget.TextView;

public class CounterTask extends AsyncTask<Integer, Integer, Integer> {
	
	TextView mOutput;
    int mCount = 0;

	
	@Override
	protected Integer doInBackground(Integer... arg0) {
		// TODO Auto-generated method stub
		for (int i = 0; i < arg0[0]; i++) {
		
            mCount++;
            publishProgress(mCount);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {;}
        }
        return mCount;

	}
	public void setOutputView(TextView txt) {
        mOutput = txt;
    }

	@Override
	protected void onCancelled() {
		// TODO Auto-generated method stub
		super.onCancelled();
	}

	@Override
	protected void onCancelled(Integer result) {
		// TODO Auto-generated method stub
		mOutput.setText("취소됨");
		super.onCancelled(result);
	}

	@Override
	protected void onPostExecute(Integer result) {
		// TODO Auto-generated method stub
		 mOutput.setText("결과: " + result);
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
        mCount = 0;
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		mOutput.setText("진행상황: " + values[0]);
		super.onProgressUpdate(values);
	}

}
