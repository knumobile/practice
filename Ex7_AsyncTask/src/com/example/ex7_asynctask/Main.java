package com.example.ex7_asynctask;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;


public class Main extends Activity {
	
	CounterTask mTask;
	boolean mBreak = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    public void startWork(View v) {
    	if (mBreak) {
    		mBreak = false;            
    		mTask = new CounterTask();
    		mTask.setOutputView( (TextView)findViewById(R.id.textView1) );
    		mTask.execute(10);
    	}else {
    		mBreak = true;
    	}
    	
    }
    
    public void cancelWork(View v) {
    	mTask.cancel(true);
    }
    

}
