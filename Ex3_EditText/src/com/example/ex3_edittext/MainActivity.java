package com.example.ex3_edittext;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		AutoCompleteTextView actext = (AutoCompleteTextView)findViewById(R.id.editText1);
        String[] words = { "apple", "appose", "application" };

        actext.setAdapter(
                new ArrayAdapter<String>(
                    this,
                    R.layout.item,
                    R.id.name,
                    words
                )
            );
        TextWatcher txtw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i("Act1",
                    "beforeTextChanged('"+s+"', "
                    +String.valueOf(start)+", "
                    +String.valueOf(count)+", "
                    +String.valueOf(after)+")");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i("Act1",
                    "onTextChanged('"+s+"', "
                    +String.valueOf(start)+", "
                    +String.valueOf(before)+", "
                    +String.valueOf(count)+")");
                
                int cnt = s.toString().length();
                Button btn = (Button)findViewById(R.id.button1);
                btn.setText(String.valueOf(cnt) + "/" + 160);

            }
            @Override
            public void afterTextChanged(Editable s) {
                Log.i("Act1", "afterTextChanged");
            }
        };
        actext.addTextChangedListener(txtw);
	}
	
	private boolean mSoftKBD = false;
	
	public void onClickButton(View v)
    {
        InputMethodManager imm =
                (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            if (mSoftKBD) {
            	imm.hideSoftInputFromWindow(findViewById(R.id.editText1).getWindowToken(), 0);
            	mSoftKBD = false;
            }
            else {
                imm.showSoftInput(findViewById(R.id.editText1), 0);
                mSoftKBD = true;
            }

		
		
		EditText edit = (EditText)findViewById(R.id.editText1);
        String input = edit.getText().toString();
    
        Toast msg = Toast.makeText(
        		MainActivity.this,
                "'" + input + "'을 입력했습니다.",
                Toast.LENGTH_SHORT);
        msg.show();
        
        edit.setText("입력 완료!");
    }

}
