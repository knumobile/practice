package com.example.ex5_localfilesharing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
     
    }
    public void onButtonClick(View v) {
        Toast msg = null;
        TextView view = (TextView)findViewById(R.id.textView1);

        try {
            Context fileTest = createPackageContext("com.example.ex5_localfilesave",
                Context.CONTEXT_IGNORE_SECURITY);

            FileInputStream fis = fileTest.openFileInput("test.txt");
            byte[] buff = new byte[fis.available()];
            while (fis.read(buff) != -1) {;}
            fis.close();

            view.setText(new String(buff));

        } catch (NameNotFoundException e) {
            msg = Toast.makeText(this,
                "파일 읽기 실패\n패키지를 찾을 수 없음",
                Toast.LENGTH_SHORT);
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            msg = Toast.makeText(this,
                "파일 읽기 실패\n파일을 찾을 수 없음",
                Toast.LENGTH_SHORT);
            e.printStackTrace();

        } catch (IOException e) {
            msg = Toast.makeText(this,
                "파일 읽기 실패\n내용을 읽을 수 없음",
                Toast.LENGTH_SHORT);
            e.printStackTrace();
        }

        if (msg == null) {
            msg = Toast.makeText(this,
                "파일 읽기 성공",
                Toast.LENGTH_SHORT);
         }
         msg.show();
         
        
         String example = "This cashe was written in onClickButton"; 
         byte[] buff = example.getBytes() ;
         File cacheDir  = getCacheDir();
         File cacheFile = new File(cacheDir.getAbsolutePath(), "view_cache");
         try {
             FileOutputStream fos = new FileOutputStream(cacheFile.getAbsolutePath());
             fos.write(buff);
             fos.close();
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }

    }
    public void onCheckSD(View v) {
        String output = "SD카드 조사";
        output += "\n현재 상태: ";
        output += Environment.getExternalStorageState();
        output += "\n마운트 경로: ";
        output += Environment.getExternalStorageDirectory().getAbsolutePath();
        output += "\n루트 경로: ";
        output += Environment.getRootDirectory().getAbsolutePath();
        output += "\n데이터 경로: ";
        output += Environment.getDataDirectory().getAbsolutePath();
        output += "\n다운로드 캐시 경로: ";
        output += Environment.getDownloadCacheDirectory().getAbsolutePath();

        TextView view = (TextView)findViewById(R.id.textView1);
        view.setText(output);
    }


}
