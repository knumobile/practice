package com.example.ex5_preferencesave;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    @Override
    public void onStart() {
        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);

        EditText txt = null;
        txt = (EditText)findViewById(R.id.editText1);
        txt.setText(pref.getString("name", "이름"));
        txt = (EditText)findViewById(R.id.editText2);
        txt.setText(pref.getString("phone", "전화 번호"));

        super.onStart();
    }
    @Override
    public void onStop() {
        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();

        EditText txt = null;
        txt = (EditText)findViewById(R.id.editText1);
        edit.putString("name", txt.getText().toString());
        txt = (EditText)findViewById(R.id.editText2);
        edit.putString("phone", txt.getText().toString());
    
        edit.commit();

        super.onStop();
    }
    
    public void onClickFinish(View v) {
        finish();
    }
  


}
