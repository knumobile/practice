package com.example.ex7_thread;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Main extends Activity {
	int mForeCount = 0;
    int mBackCount = 0;
    boolean mBreak = true;

    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                showCount();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void startWork(View v) {
        if (mBreak) {
            mBreak = false;
            Button start = (Button)findViewById(R.id.button1);
            start.setText("작업 중지!");

          Thread thread = new Thread(new BackCounter());
            thread.setDaemon(true);
            thread.start();
        } else {
            mBreak = true;
            Button start = (Button)findViewById(R.id.button1);
            start.setText("작업 시작!");
        }
    }
    class BackCounter implements Runnable {
        public void run() {
            while (!mBreak) {
                mBackCount++;
                mHandler.sendEmptyMessage(1);

                Log.i("Main", "BackCount: " + mBackCount);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) { ; }
            } } }
    public void showCount() {
        String output = "카운트 증가";
        output += "\nForeCount: " + mForeCount;
        output += "\nBackCount: " + mBackCount;
        TextView txt = (TextView)findViewById(R.id.textView1);
        txt.setText(output);
    }

    public void onClickButton(View v) {
        mForeCount++;
        showCount();
    }

}

