package com.example.ex3_spinner;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		Spinner spin = (Spinner)findViewById(R.id.spinner1);

        ArrayAdapter<CharSequence> adapName =
            ArrayAdapter.createFromResource(
                this,
                R.array.company,
                android.R.layout.simple_spinner_item
            );
        adapName.setDropDownViewResource(
            android.R.layout.simple_spinner_dropdown_item);
        
        spin.setAdapter(adapName);

        spin.setOnItemSelectedListener(
                new OnItemSelectedListener() {
                    public void onItemSelected( AdapterView<?> parent, View v, int pos, long id) {
                        Log.i("Act1.spinner1", "pos = " + pos + ", id = " + id);
                    }
                    public void onNothingSelected( AdapterView<?> parent) {
                        Log.i("Act1.spinner1", "���� ����");
                    }
                }
            );
		/*
Spinner spin = (Spinner)findViewById(R.id.spinner1);
        
        ArrayList<String> arrName = new ArrayList<String>();
        arrName.add("Android");
        arrName.add("Apple");
        arrName.add("Nokia");
        arrName.add("RIM");
        arrName.add("Microsoft");        
        ArrayAdapter<String> adapName = new ArrayAdapter<String>(
            this,
            R.layout.item,
            R.id.name,
            arrName
        );
        
        spin.setAdapter(adapName);
	*/
	}
	
}
