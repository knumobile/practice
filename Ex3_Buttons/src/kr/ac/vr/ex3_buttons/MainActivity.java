package kr.ac.vr.ex3_buttons;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class MainActivity extends Activity {
	
    private RadioGroup mRGrp1;
    private Button mBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		/*
        final Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast msg = Toast.makeText(
                MainActivity.this,
                "일반 버튼이 눌렸습니다.",
                Toast.LENGTH_SHORT);
                msg.show();
            }
        });
		 */
        mRGrp1 = (RadioGroup)findViewById(R.id.radioGroup1);
        mBtn = (Button)findViewById(R.id.button1);
        
        mRGrp1.setOnCheckedChangeListener(
            new OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup grp, int chkId) {
                    if (chkId == View.NO_ID) {
                        mBtn.setText("일반 버튼");
                    }
                    else {
                        mBtn.setText(
                            chkId + " 라디오 버튼 선택됨"
                            );
                    }
                }
            }
        );
	}
	
	public void onClickButton(View v) {
		if (v == findViewById(R.id.button1))
        {
            mRGrp1.clearCheck();
			Toast msg = Toast.makeText(
                    this,
                    "일반 버튼이 눌렸습니다.",
                    Toast.LENGTH_SHORT);
            msg.show();
        }
        else if (v == findViewById(R.id.checkBox1))
        {
            Toast msg = Toast.makeText(
                    this,
                    ( ((CheckBox)v).isChecked() ?
                        "체크박스가 활성되었습니다." :
                        "체크박스가 비활성되었습니다." ),
                    Toast.LENGTH_SHORT);
            msg.show();
        }

    }
	/*
	public void onClickRadio(View v) {
        RadioButton rbtn = (RadioButton)v;
        Button btn = (Button)findViewById(R.id.button1);
        btn.setText( rbtn.getText() + "을(를) 좋아합니다." );
    }
	*/
	
	

}
