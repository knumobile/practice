package com.example.ex7_bc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void onClickSend(View v) {
        Intent i = new Intent();
        i.setAction("com.broadcast.ANDROID");
      
        
        sendBroadcast(i);
        
    }	

   
}
