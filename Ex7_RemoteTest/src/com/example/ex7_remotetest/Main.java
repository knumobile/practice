package com.example.ex7_remotetest;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.Toast;

import com.example.ex7_remote.ICalc;


public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    com.example.ex7_remote.ICalc mCalc;

    ServiceConnection srvConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mCalc = null;
        }
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            mCalc = ICalc.Stub.asInterface(arg1);
        }
    };

    public void onResume() {
        super.onResume();
        Intent i = new Intent("com.service.REMOTE_CALC");
        bindService(i, srvConn, BIND_AUTO_CREATE);
    }
    
    public void onPause() {
        super.onPause();
        unbindService(srvConn);
    }
    public void callRemoteAdd(View v) {
        try {
            int ret = mCalc.add(11, 6);
            Toast.makeText(this, "11 + 6 = " + ret, Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    
    public void callRemoteMultiply(View v) {
        try {
            int ret = mCalc.multiply(11, 6);
            Toast.makeText(this, "11 * 6 = " + ret, Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

   
}
