package com.example.ex7_remote;

interface ICalc {
	int add(in int a, in int b);
	int multiply(in int a, in int b);
}
