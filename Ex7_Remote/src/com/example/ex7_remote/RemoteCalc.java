package com.example.ex7_remote;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

public class RemoteCalc extends Service {
    
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	ICalc.Stub mBinder = new ICalc.Stub() {

        @Override
        public int multiply(int a, int b) throws RemoteException {
            return (a * b);
        }

        @Override
        public int add(int a, int b) throws RemoteException {
            return (a + b);
        }
    };


}
