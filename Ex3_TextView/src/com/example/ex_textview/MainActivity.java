package com.example.ex_textview;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        TextView text4 = (TextView)findViewById(R.id.textView4);
        text4.setText(Html.fromHtml("<b>Bold</b> <i>Italic</i> <u>Underline</u> <a href='http://vr.knu.ac.kr'>Link</a>"));

		
		
	}

	
}
