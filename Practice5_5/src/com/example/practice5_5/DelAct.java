package com.example.practice5_5;

import android.app.*;
import android.database.sqlite.*;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

public class DelAct extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
        setContentView(R.layout.delact);
	
	    // TODO Auto-generated method stub
	}

    public void onClickButton(View v) {
    	String name = ((EditText) findViewById(R.id.editText1)).getText().toString();

        String sql = "DELETE FROM people WHERE name='" + name + "';";
        SQLiteDatabase db = openOrCreateDatabase(
            "test.db",
            SQLiteDatabase.CREATE_IF_NECESSARY,
            null
        );
        db.execSQL(sql);
        finish();
    }

}
