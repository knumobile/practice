package com.example.practice5_5;

import android.app.*;
import android.database.sqlite.*;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

public class InsAct extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
        setContentView(R.layout.insact);
	
	    // TODO Auto-generated method stub
	}

    public void onClickButton(View v) {
    	String name = ((EditText) findViewById(R.id.editText1)).getText().toString();
    	String age = ((EditText) findViewById(R.id.editText2)).getText().toString();

        String sql = "INSERT INTO people (name,age) VALUES ('" + name + "'," + age + ");";
        SQLiteDatabase db = openOrCreateDatabase(
            "test.db",
            SQLiteDatabase.CREATE_IF_NECESSARY,
            null
        );
        db.execSQL(sql);
        finish();
    }
}
