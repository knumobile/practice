package com.example.practice5_5;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class SqlAct extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
        setContentView(R.layout.sqlact);
	
	    // TODO Auto-generated method stub
	}

    public void onClickButton(View v) {
    	String sql = ((EditText) findViewById(R.id.editText1)).getText().toString();
    	
        SQLiteDatabase db = openOrCreateDatabase(
            "test.db",
            SQLiteDatabase.CREATE_IF_NECESSARY,
            null
        );
        db.execSQL(sql);
        finish();
    }

}
