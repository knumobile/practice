package com.example.ex3_gallery;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

@SuppressWarnings("deprecation")
public class MainActivity extends Activity {

	@Override
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	    
		Gallery gall = (Gallery)findViewById(R.id.gallery1);
        
        ImageAdapter galAdapter = new  ImageAdapter(this);
        gall.setAdapter(galAdapter);
	}
	Integer[] mImageId = { R.drawable.logo1, R.drawable.logo2,
            R.drawable.logo3, R.drawable.logo4 };

	public class ImageAdapter extends BaseAdapter {
	    private Context mContext;

	    public ImageAdapter(Context c) {
	        mContext = c;
	    }
	 
	  
		public View getView(int pos, View v, ViewGroup p) {
	        ImageView img = new ImageView(mContext);
	    
	        img.setImageResource(mImageId[pos]);
	        img.setScaleType(ImageView.ScaleType.FIT_XY);
	        img.setLayoutParams(new Gallery.LayoutParams(200,140));

	        return img;
	    }

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}
	}
}
