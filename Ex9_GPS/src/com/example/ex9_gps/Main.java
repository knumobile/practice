package com.example.ex9_gps;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.EditText;


public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        /*
        LocationManager locationManager =
                (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                showNewLocation(location);
            }
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
            public void onProviderEnabled(String provider) {
            }
            public void onProviderDisabled(String provider) {
            }
        };

        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    
    }
    private Location bestLocation = null;
    
    public void showNewLocation(Location location) {
    	if (isBetterLocation(location, bestLocation)) {
            bestLocation = location;
        }
        location = bestLocation;
    	
    	// Provider
        EditText txt = (EditText)findViewById(R.id.editText1);
        txt.setText(location.getProvider());

        // Accuracy
        txt = (EditText)findViewById(R.id.editText2);
        if (location.hasAccuracy()) {
            txt.setText(" " + location.getAccuracy());
        } else {
            txt.setText("Unknown");
        }

        // Longitude
        txt = (EditText)findViewById(R.id.editText3);
        txt.setText(" " + location.getLongitude());

        // Latitude
        txt = (EditText)findViewById(R.id.editText4);
        txt.setText(" " + location.getLatitude());
     // Altitude
        txt = (EditText)findViewById(R.id.editText5);
        if (location.hasAltitude()) {
            txt.setText(" " + location.getAltitude());
        } else {
            txt.setText("Unknown");
        }

        // Time
        txt = (EditText)findViewById(R.id.editText6);
        txt.setText(" " + location.getTime());

        // Bearing
        txt = (EditText)findViewById(R.id.editText7);
        if (location.hasBearing()) {
            txt.setText(" " + location.getBearing());
        } else {
            txt.setText("Unknown");
        }
        // Speed
        txt = (EditText)findViewById(R.id.editText8);
        if (location.hasSpeed()) {
            txt.setText(" " + location.getSpeed());
        } else {
            txt.setText("Unknown");
        }

        // Extras
        txt = (EditText)findViewById(R.id.editText9);
        if (location.getExtras() != null) {
            txt.setText(location.getExtras().toString());
        } else {
            txt.setText("None");
        }
    }
        private static final int TWO_MINUTES = 1000 * 60 * 2;

        protected boolean isBetterLocation(Location location, Location currentBestLocation) {
            if (currentBestLocation == null) {
                return true;
            }

            long timeDelta = location.getTime() - currentBestLocation.getTime();
            boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
            boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
            boolean isNewer = timeDelta > 0;

            if (isSignificantlyNewer) {
                return true;
            } else if (isSignificantlyOlder) {
                return false;
            }

            int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
            boolean isLessAccurate = accuracyDelta > 0;
            boolean isMoreAccurate = accuracyDelta < 0;
            boolean isSignificantlyLessAccurate = accuracyDelta > 200;

            boolean isFromSameProvider = isSameProvider(location.getProvider(),
                    currentBestLocation.getProvider());
            if (isMoreAccurate) {
                return true;
            } else if (isNewer && !isLessAccurate) {
                return true;
            } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
                return true;
            }
            return false;
        }

        private boolean isSameProvider(String provider1, String provider2) {
            if (provider1 == null) {
              return provider2 == null;
            }
            return provider1.equals(provider2);
        */
        }    
        
}
