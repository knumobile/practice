package com.example.ex7_msgloop;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.example.ex7_msgloop.CalcThread;

public class Main extends Activity {
    
	CalcThread mThread;
	
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            String output = "";

            switch (msg.what) {
            case 1:// ���� ���
                output = "���� ���";
                output += "\n������: " + msg.arg1;
                output += "\n����: " + msg.arg2;
              
                break;

            case 2:// ������ ���
                output = "������ ���";
                output += "\n������: " + msg.arg1;
                output += "\n������: " + ((Double)msg.obj).doubleValue();
                break;
            }

            TextView txt = (TextView)findViewById(R.id.textView2);
            txt.setText(output);
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    
        mThread = new CalcThread(mHandler);
        mThread.setDaemon(true);
        mThread.start();

    }
    public void calcNumber(View v) {
        TextView txt = (TextView)findViewById(R.id.textView2);
        EditText edit = (EditText)findViewById(R.id.editText1);
        int num = Integer.parseInt(edit.getText().toString());

        Message msg;
        switch (v.getId()) {
        case R.id.button1:
            msg = Message.obtain(mHandler, 1, num, 0);
            mThread.getHandler().sendMessage(msg);
            txt.setText("���� ��� ��...");
            break;
    
        case R.id.button2:
            msg = Message.obtain(mHandler, 2, num, 0);
            mThread.getHandler().sendMessage(msg);
            
            txt.setText("������ ��� ��...");
            break;
        }
    }



   
}
