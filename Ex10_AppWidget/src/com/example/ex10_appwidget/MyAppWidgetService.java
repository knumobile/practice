package com.example.ex10_appwidget;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;

public class MyAppWidgetService extends Service {
	public static final String UPDATE = "Update MyAppWidget";

    @Override
    public void onStart(Intent intent, int startId) {
        String command = intent.getAction();

        int appWidgetId =
            intent.getExtras().getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);
        AppWidgetManager appWidgetManager =
            AppWidgetManager.getInstance(getApplicationContext());

        if (command.equals(UPDATE)) {
            MyAppWidgetProvider.updateCount(
                    getApplicationContext().getPackageName(),
                    appWidgetManager, appWidgetId);
        }
        //super.onStartCommand(intent, 0, startId);
        super.onStart(intent, startId);
    }

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}	
