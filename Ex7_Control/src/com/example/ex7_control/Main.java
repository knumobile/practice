package com.example.ex7_control;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Main extends Activity {
	
    //?
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void startNews(View v) {
        Intent i = new Intent("com.service.FAKE_NEWS");
        startService(i);
    }
    
    public void stopNews(View v) {
        Intent i = new Intent("com.service.FAKE_NEWS");
        stopService(i);
    }
    
    
    
   
}
