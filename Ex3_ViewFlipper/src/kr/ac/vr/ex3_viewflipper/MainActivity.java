package kr.ac.vr.ex3_viewflipper;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

public class MainActivity extends Activity {

	ViewFlipper mFlip = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mFlip = (ViewFlipper)findViewById(R.id.viewFlipper1);
        mFlip.setOnClickListener(
            new OnClickListener() {
                public void onClick(View v) {
                    int childId  = mFlip.getDisplayedChild();
                    int childCnt = mFlip.getChildCount();

                    if (childId + 1 == childCnt) {
                        mFlip.setDisplayedChild(0);
                    }
                    else {
                        mFlip.setDisplayedChild(childId + 1);
                    }
                }
            }
        );
    }
    
    public void onClick(View v) {
        int childId  = mFlip.getDisplayedChild();
        int childCnt = mFlip.getChildCount();

        mFlip.setInAnimation(
            AnimationUtils.loadAnimation(
            		MainActivity.this,
                android.R.anim.slide_in_left )
            );
        mFlip.setInAnimation(
            AnimationUtils.loadAnimation(
            		MainActivity.this,
                android.R.anim.slide_out_right )
            );

        if (childId + 1 == childCnt) {
            mFlip.setDisplayedChild(0);
        }
        else {
            mFlip.showNext();
        }
    }

}
