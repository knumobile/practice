package com.example.ex5_rawresourcedatasave;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	

        try {

            InputStream is = getResources().openRawResource(R.raw.rawtext);
            byte[] data = new byte[is.available()];
            while (is.read(data) != -1) {;}
            is.close();

            TextView txt = (TextView)findViewById(R.id.textView1);
            txt.setText(new String(data));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	
}

	

